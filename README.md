# Applications Android pour les services C.H.A.T.O.N.S.

- _[click here](./README-EN.md) for the English version_  
- _[cliquez ici](./README.md) pour la version en Français_  
### Constat
Malgré que les applications soit ouvertes (open source); voire parfois libre (free software), certaines d'entre-elles sont distribuées via les canaux traditionnaux tel que G\*\*glePlay et AppleSt\*re.

L'idéé est de sensibiliser les mainteneurs de ces applications afin qu'ils rendent accessible leurs applications par d'autres canaux. Par exemple; ils peuvent publier l'APK sur leur site Internet ou via le rendre accessible via F-Droid.

## Le but
En attendant cette conscientisation, les APK (fichier d'installation pour Android) seront accessibles via ce dépôts.

### Comment participer

1. Utiliser ce dépôt. (voir la [liste des apps](./apk/README-FR.md))

2. Ajouter une étoile à ce dépôt.

3. Partager ce dépôt.

4. Prenez 5 minutes pour envoyer un courriel aux mainteneurs pour lui demander de proposer via F-Droid. (voir la [liste des lettres types](https://))

5. M'aider à maintenir ce dépôt (voir [comment extraire](https: //))

## TODO
- Promouvoir ce dépôt
- Courriel type pour demander au mainteneur de publier leurs apps, soit sur leur site ou via F-Droid.
    - en français
    - in english