# Listes des applications
- [Jitsi Meet](./org.jitsi.meet.apk) : pour JitsiMeet, Framatalk... (dernière mise à jour = 6 mars 2017)
- [Mattermost](./com.mattermost.mattermost.apk) : pour Mattermost, Framateam... (dernière mise à jour = 16 janvier 2017)
- [Tusky](./com.keylesspalace.tusky.apk) : pour Mastodon (dernière mise à jour = 2 avril 2017)

## Listes des applications disponibles via [F-Droid](https://f-droid.org/)
- [DavDroid](https://f-droid.org/repository/browse/?fdfilter=DAVdroid&fdid=at.bitfire.davdroid) : pour synchroniser vos contacts et calendriers.
- [Dandelion*](https://f-droid.org/repository/browse/?fdfilter=diaspora&fdid=com.github.dfa.diaspora_android) : à utiliser avec Diaspora, Framasphère...
- [Diaspora Web Client](https://f-droid.org/repository/browse/?fdfilter=diaspora&fdid=com.voidcode.diasporawebclient) : à utiliser avec Diaspora, Framasphère...
- [FBReader](https://f-droid.org/repository/browse/?fdfilter=fbreader&fdid=org.geometerplus.zlibrary.ui.android) : pour synchroniser et lire vos libres numériques.
- [Goblim](https://f-droid.org/repository/browse/?fdfilter=goblim&fdid=fr.mobdev.goblim) : permet de partager vos images via Lutim, Framapic,...
-  [Padland](https://f-droid.org/repository/browse/?fdfilter=pad&fdid=com.mikifus.padland) : à utiliser avec Etherpad, FramaPad...
-  [MGit](https://f-droid.org/repository/browse/?fdfilter=git&fdid=com.manichord.mgit&fdpage=4) : à utiliser avec Gitlab, Framagit,...
-  [Minetest](https://f-droid.org/repository/browse/?fdfilter=minetest&fdid=net.minetest.minetest) : 
-  [Minetest Mods](https://f-droid.org/repository/browse/?fdfilter=minetest&fdid=com.rubenwardy.minetestmodmanager) : 
-  [Nextcloud Client](https://f-droid.org/repository/browse/?fdfilter=nextcloud&fdid=com.nextcloud.client) : pour synchroniser tes fichiers avec Nextcloud, ownCloud, Framadrive...
-  [OSMand~](https://f-droid.org/repository/browse/?fdfilter=openstreetmap&fdid=net.osmand.plus) : pour utiliser OpenStreetMap, framacarte...
- [OpenTask](https://f-droid.org/repository/browse/?fdfilter=DAVdroid&fdid=org.dmfs.tasks) : pour synchroniser vos tâches.
-  [Seafile](https://f-droid.org/repository/browse/?fdfilter=seafile&fdid=com.seafile.seadroid2)
-  [TTRSS-Reader](https://f-droid.org/repository/browse/?fdfilter=ttrss&fdid=org.ttrssreader) : pour utiliser Tiny Tiny RSS, Framanews,...
-  [Wallabag](https://f-droid.org/repository/browse/?fdfilter=wallabag&fdid=fr.gaulupeau.apps.InThePoche)

### Mention spécial
Puisque les C.H.A.T.O.N.S. promeut l'autonomie de chacun, j'ai voulu mentionner ces applications qui ne font pas partie des services offerts par les C.H.A.T.O.N.S mais qui, aideront sans doute, le bon mainteint de ceux-ci.
- [Open Mentoring](https://f-droid.org/repository/browse/?fdfilter=git&fdid=org.iilab.openmentoring&fdpage=4) : Contenu pédagogique pratique sur la sécurité numérique.


* * *
P.S.: Si vous ajoutez une application, prière de la classer par ordre alphabétique ainsi que d'écrire la date de la dernière mise à jour.
##### EXEMPLE:
![](../img/exemple_app_version.png)
