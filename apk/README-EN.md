# List of applications
- [Jitsi Meet](./org.jitsi.meet.apk) : for JitsiMeet, Framatalk... (last updated = March 6, 2017)
- [Mattermost](./com.mattermost.mattermost.apk) : for Mattermost, Framateam... (last updated = January 16, 2017)
- [Tusky](./com.keylesspalace.tusky.apk) : for Mastodon (last updated = April 2, 2017)

## List of applications available via [F-Droid](https://f-droid.org/)
- [DavDroid](https://f-droid.org/repository/browse/?fdfilter=DAVdroid&fdid=at.bitfire.davdroid) : to synchronise your contacts and calendars.
- [Dandelion*](https://f-droid.org/repository/browse/?fdfilter=diaspora&fdid=com.github.dfa.diaspora_android) : to use with Diaspora, Framasphere...
- [Diaspora Web Client](https://f-droid.org/repository/browse/?fdfilter=diaspora&fdid=com.voidcode.diasporawebclient) : to use with Diaspora, Framasphere...
- [FBReader](https://f-droid.org/repository/browse/?fdfilter=fbreader&fdid=org.geometerplus.zlibrary.ui.android) : to synchronise and read your ebook.
- [Goblim](https://f-droid.org/repository/browse/?fdfilter=goblim&fdid=fr.mobdev.goblim) : help you to share your images on Lutim, Framapic,...
-  [Padland](https://f-droid.org/repository/browse/?fdfilter=pad&fdid=com.mikifus.padland) : to use Etherpad, FramaPad...
-  [MGit](https://f-droid.org/repository/browse/?fdfilter=git&fdid=com.manichord.mgit&fdpage=4) : to use with Gitlab, Framagit,...
-  [Minetest](https://f-droid.org/repository/browse/?fdfilter=minetest&fdid=net.minetest.minetest) : 
-  [Minetest Mods](https://f-droid.org/repository/browse/?fdfilter=minetest&fdid=com.rubenwardy.minetestmodmanager) : 
-  [Nextcloud Client](https://f-droid.org/repository/browse/?fdfilter=nextcloud&fdid=com.nextcloud.client) : to sync your files with Nextcloud, ownCloud, Framadrive...
-  [OSMand~](https://f-droid.org/repository/browse/?fdfilter=openstreetmap&fdid=net.osmand.plus) : to use OpenStreetMap, framacarte...
- [OpenTask](https://f-droid.org/repository/browse/?fdfilter=DAVdroid&fdid=org.dmfs.tasks) : to synchronise your task.
-  [Seafile](https://f-droid.org/repository/browse/?fdfilter=seafile&fdid=com.seafile.seadroid2)
-  [TTRSS-Reader](https://f-droid.org/repository/browse/?fdfilter=ttrss&fdid=org.ttrssreader) : to use Tiny Tiny RSS, Framanews,...
-  [Wallabag](https://f-droid.org/repository/browse/?fdfilter=wallabag&fdid=fr.gaulupeau.apps.InThePoche) : to use with Framabag...

### Special Mention
As C.H.A.T.O.N.S. promotes the autonomy of each one, I want to mention those applications, which are not part of the services offered by the C.H.A.T.O.N.S, but which will probably help, you and them, to be better.
- [Open Mentoring](https://f-droid.org/repository/browse/?fdfilter=git&fdid=org.iilab.openmentoring&fdpage=4) : Practical educational content on digital security.

* * *
P.S.: If you add an application, please sort it in alphabetical order and write the date of the last update.
##### EXEMPLE:
![](../img/exemple_app_version.png)