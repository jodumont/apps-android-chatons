# Android Apps for C.H.A.T.O.N.S.

- _[click here](./README-EN.md) for the English version_  
- _[cliquez ici](./README.md) pour la version en Français_  

### Constat

Despite open source applications; and sometimes even free software, some of them are only distributed through traditional channels such as G\*\*glePlay or AppleSt\*re.

The idea is to sensitize the maintainers, of these applications, that they may make their applications accessible through other channels. Such as; They can publish the APK on their website or make it accessible via F-Droid.

## The Goal
In the meantime, the APK (installation file for Android) will be accessible via this repository.

### How to participate

1. Use this repository. (See the [apps list](./apk/README-EN.md))

2. Add a star to this repository.

3. Share this repositoy.

4. Take 5 minutes to e-mail the maintainers about asking him to propose through F-Droid. (See [list of type letters] (https: //))

5. Help me maintain the repository (see [How to extract](https: //))

## TODO
- Promote this repository
- Form Email to ask maintainers to make their apps available on their site or F-Droid.
    - en français
    - in english